package com.example.demospring1.login.model.dto;

import com.example.demospring1.login.model.enity.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserCaseResponse {
    private List<User> user;
}
