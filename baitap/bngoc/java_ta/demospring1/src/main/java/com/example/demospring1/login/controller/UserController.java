package com.example.demospring1.login.controller;

import com.example.demospring1.login.model.base.BaseResponse;
import com.example.demospring1.login.model.dto.UserCaseResponse;
import com.example.demospring1.login.model.dto.UserRegistration;
import com.example.demospring1.login.model.enity.User;
import com.example.demospring1.login.repository.UserRepository;
import com.example.demospring1.login.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class UserController {
    @Autowired
    UserService userService;
    @Autowired
    UserRepository userRepository;
    @GetMapping("/login1")
    public String register(
            User user,
        @RequestParam( value = "username", required = false) String username,
        @RequestParam( value = "firstname", required = false) String firstname,
        @RequestParam( value = "lastname", required = false) String lastname,
        @RequestParam( value = "password", required = false) String password,
        Model model){
        model.addAttribute("username", user.getUsername());
        model.addAttribute("firstname", user.getFirstname());
        model.addAttribute("lastname", user.getLastname());
        model.addAttribute("password", user.getPassword());
        return "login1";
    }

    @PostMapping("/processregister")
    public String processRegister (User user){
        userRepository.save(user);
        return "user";
    }
//    @RequestMapping("user")
//    public ResponseEntity<BaseResponse<UserCaseResponse>> findUser() {
//        List<User> user = userService.findUser();
//        BaseResponse<UserRepository> baseResponse = new BaseResponse<>();
//        baseResponse.setCode(1);
//        baseResponse.setMessage("success");
//        baseResponse.setData(UserRegistration.builder()
//                .user(user)
//                .build());
//        return ResponseEntity.ok(baseResponse);
//    }
}
