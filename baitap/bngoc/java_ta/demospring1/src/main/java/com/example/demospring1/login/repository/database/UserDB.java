package com.example.demospring1.login.repository.database;

import com.example.demospring1.login.model.enity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface UserDB extends JpaRepository<User, Integer> {
    @Query(value = "SELECT * FROM user where username= ?1 AND firstname = ?2 AND lastname = ?3 password = ?4", nativeQuery = true)
    User findUser(String username,String firstname, String lastname ,String password);

    @Query(value = "insert into user (username, firstname, lastname, password) values (?1, ?2, ?3, ?4)",nativeQuery = true)
    User insertNewUser(String username ,String firstname, String lastname, String password);

    @Override
    <S extends User> S save(S s);


}
