package com.example.demospring1.login.service;

import com.example.demospring1.login.model.dto.UserRegistration;
import com.example.demospring1.login.model.enity.User;
import com.example.demospring1.login.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {
    @Autowired
    UserRepository userRepository;

    public List<User> findAllUser(){
        return userRepository.findAllUser();
    }
    public User findById(Integer id){
        return userRepository.findById(id);
    }
    public User findUser(String username, String firstname, String lastname, String password){
        return userRepository.findUser(username , firstname, lastname, password);
    }
    public User insertNewUser(String username, String firstname, String lastname, String password){
        return userRepository.insertNewUser(username , firstname, lastname, password);
    }
    public int createUser(UserRegistration userRegistration){
        return userRepository.saveUser(User.builder()
                .username(userRegistration.getUsername())
                .firstname(userRegistration.getFirstName())
                .lastname(userRegistration.getLastName())
                .password(userRegistration.getPassword())
                .build());
    }
}
