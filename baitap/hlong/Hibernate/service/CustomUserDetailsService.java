package com.example.Hibernate.service;

import com.example.Hibernate.model.entity.User;
import com.example.Hibernate.repository.database.UserDB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private UserDB userDB;


    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

        User user = userDB.findByEmail(email);
        if(email==null){
            throw new UsernameNotFoundException("Email Not Found");
        }

        return null;
    }
}
