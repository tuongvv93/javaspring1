package com.example.Hibernate.service;

import com.example.Hibernate.model.entity.Book;
import com.example.Hibernate.repository.database.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookService {
    @Autowired
    private BookRepository bookRepository;

    //Tạo book mới
    public Book createBook (Book book){
        return bookRepository.save(book);
    }

    //Hiển thị danh sách book
    public List<Book> getBooks(){
        return bookRepository.findAll();
    }

    //Hiển thị Book theo id
    public Book findBookById(int id){
        return bookRepository.findById(id).orElse(null);
    }

    //Hiển thị Book theo tên
    public Book findByName(String name){
        return bookRepository.findByName(name);
    }

    //Sửa Book
    public Book updateBook(Book Book) {
        Book existingBook = bookRepository.findById(Book.getId()).orElse(null);
        existingBook.setName(Book.getName());
        existingBook.setPrice(Book.getPrice());
        return bookRepository.save(existingBook);
    }

    //Xóa Book theo id
    public String deleteBookById(int id) {
        bookRepository.deleteById(id);
        return "Book removed !! " + id;
    }
}
