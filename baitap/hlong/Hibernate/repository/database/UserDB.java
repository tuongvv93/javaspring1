package com.example.Hibernate.repository.database;

import com.example.Hibernate.model.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface UserDB extends JpaRepository<User,Long> {
    @Query("select u from User u where u.email =?1")
    User findByEmail(String email);
}
