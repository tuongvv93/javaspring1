package com.example.Hibernate.controller;

import com.example.Hibernate.model.entity.User;
import com.example.Hibernate.repository.database.UserDB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Controller
public class UserController {

    @Autowired
    private UserDB userDB;

    @GetMapping("/homepage")
    public String home(){
        return "homepage";
    }

//    @GetMapping("/register")
//    public String showSignUpForm(Model model){
//        model.addAttribute("user",new User());
//        return "signup_form";
//    }

    @GetMapping("/register")
    public  String admin(Model model){
        model.addAttribute("user",new User());
        return "signup_form";
    }

    @PostMapping("/process_register")
    public String processRegistration(User user){
        userDB.save(user);
        return "register_success";
    }

}
