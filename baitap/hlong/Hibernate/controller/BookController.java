package com.example.Hibernate.controller;

import com.example.Hibernate.model.entity.Book;
import com.example.Hibernate.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class BookController {
    @Autowired
    BookService bookService;

    //Hiển thị danh sách book
    @GetMapping("/getBooks")
    public List<Book> getBooks(){
        return bookService.getBooks();
    }

    //Hiển thị Book theo id
    @GetMapping("/bookById/{id}")
    public Book findBookById (@PathVariable int id){
        return bookService.findBookById(id);
    }

    //Hiển thị Book thoe tên
    @GetMapping("/findByName/{name}")
    public Book findByName (@PathVariable String name){
        return bookService.findByName(name);
    }

    //Tạo book mới
    @PostMapping("/addBook")
    public Book addBook (@RequestBody Book book){
        return bookService.createBook(book);
    }

    //Sửa book theo id
    @PutMapping("/updateBook")
    public Book updateBook(@RequestBody Book book){
        return bookService.updateBook(book);
    }

    //Xóa book theo id
    @DeleteMapping("/deleteBook/{id}")
    public String deleteBook (@PathVariable int id){
        return bookService.deleteBookById(id);
    }

}
