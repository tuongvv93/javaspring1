package com.example.quangbaitap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QuangbaitapApplication {

    public static void main(String[] args) {
        SpringApplication.run(QuangbaitapApplication.class, args);
    }

}
