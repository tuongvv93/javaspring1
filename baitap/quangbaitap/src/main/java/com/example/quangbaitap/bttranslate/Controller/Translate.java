package com.example.quangbaitap.bttranslate.Controller;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

@Controller
public class Translate {
    @GetMapping("translate")
    public String ggTranslate(
            @RequestParam(value = "en",required = false) String text,
            Model model
    ) {
        if(text == null || text.isEmpty()) {
            return "Translate";
        }
        final String uri = "https://translate.googleapis.com/translate_a/single?client=gtx&sl=en&tl=vi&hl=en-US&dt=t&dt=bd&dj=1&source=icon&tk=549094.549094&q="+text;
        RestTemplate restTemplae = new RestTemplate();
        String result = restTemplae.getForObject(uri, String.class);
        JSONObject obj = new JSONObject(result);
        JSONArray arr = (JSONArray) obj.get("sentences");
        obj = (JSONObject) arr.get(0);
        model.addAttribute("vi", obj.get("trans"));
        return "Translate";
    }
}
