package com.example.demospringv1.login.loginrepository;

import com.example.demospringv1.login.loginmodel.entity.LoginT;
import com.example.demospringv1.login.loginrepository.database.LoginDB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;

@Repository
public class LoginRepository {
    @Autowired
    LoginDB loginDB;
    @PersistenceContext
     private EntityManager entityManager;

    public List<LoginT> findLogin() {
        return loginDB.findAll();
    }
    @Transactional
    public int register(LoginT loginT) {
        EntityManager entity = entityManager.getEntityManagerFactory().createEntityManager();
        try {
            entity.getTransaction().begin();
            String sql ="insert into logint (username, password, firstname, lastname) values (:username, :password, :firstname, :lastname)";
            Query query = entity.createNativeQuery(sql,LoginT.class);
            query.setParameter("username", loginT.getUsername());
            query.setParameter("password", loginT.getPassword());
            query.setParameter("firstname", loginT.getFirstname());
            query.setParameter("lastname", loginT.getLastname());
            query.executeUpdate();
            Query query1 = entity.createNativeQuery("select LAST_INSERT_ID()");
            int id = Integer.parseInt(query1.getSingleResult().toString());
            entity.getTransaction().commit();
            return id;
        } catch (Exception e) {
            entity.getTransaction().rollback();
        }
        return -1;
    }

//    public LoginT findBy(String username, String password) {
//        return loginDB.findBy();
//    }
//    @Transactional
//    public int login(LoginT loginT){
//        EntityManager entity = entityManager.getEntityManagerFactory().createEntityManager();
//        try {
//            entity.getTransaction().begin();
//            Query query = entity.createNativeQuery("select c from logint where c.username = username and c.password = password");
//            int id = Integer.parseInt(query.getSingleResult().toString());
//            entity.getTransaction().commit();
//            return id;
//        }
//        catch (Exception e){
//        }
//        return -1;
//    }
}
