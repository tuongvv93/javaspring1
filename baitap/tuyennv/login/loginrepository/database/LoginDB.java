package com.example.demospringv1.login.loginrepository.database;

import com.example.demospringv1.login.loginmodel.entity.LoginT;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface LoginDB extends JpaRepository<LoginT, Integer> {
//    LoginT findBy();
}
