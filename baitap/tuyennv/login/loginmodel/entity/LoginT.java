package com.example.demospringv1.login.loginmodel.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class LoginT {
    @Id
    @GeneratedValue( strategy = GenerationType.AUTO)
    private int Id;
    private String username;
    private String password;
    private String firstname;
    private String lastname;

    public LoginT(String username, String password, String firstname, String lastname) {
    }
}
