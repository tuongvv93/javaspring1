package com.example.demospringv1.login.loginmodel.base;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoginBaseRespose <T>{
    private  int code;
    private  String message;
    private  T data;
}
