package com.example.demospringv1.login.loginservice;
import com.example.demospringv1.login.loginmodel.entity.LoginT;
import com.example.demospringv1.login.loginrepository.LoginRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LoginService {
    @Autowired
    LoginRepository loginRepository;

    public List<LoginT> findLogin() {
        return loginRepository.findLogin();
    }
//
//    public LoginT loginT(String  username, String password){
//        LoginT loginT = loginRepository.findBy(username, password);
//        return loginT;
//    }
}
