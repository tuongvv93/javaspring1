package com.example.demospringv1.login.logincontroller;

import com.example.demospringv1.login.loginmodel.base.LoginBaseRespose;
import com.example.demospringv1.login.loginmodel.entity.LoginResponse;
import com.example.demospringv1.login.loginmodel.entity.LoginT;
import com.example.demospringv1.login.loginrepository.LoginRepository;
import com.example.demospringv1.login.loginservice.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.tags.Param;

import java.util.List;
import java.util.ResourceBundle;

@Controller
@RequestMapping("")
public class LoginController {
    @Autowired
    LoginService loginService;
    @Autowired
    LoginRepository loginRepository;
    @GetMapping("/new")
    public String login(Model model){
        return "new";
    }

    @GetMapping("/register")
    public String register(
            @RequestParam(value = "username", required = false) String username,
            @RequestParam(value = "password", required = false) String password,
            @RequestParam(value = "firstname", required = false) String firstname,
            @RequestParam(value = "lastname", required = false) String lastname,
            Model model){
        LoginT loginT = new LoginT(username, password, firstname, lastname);
        model.addAttribute("logint" ,loginT);
        return "register";
    }

    @PostMapping("/process-register")
    public String processRegister (LoginT loginT){
        loginRepository.register(loginT);
        return "login";
    }

    @RequestMapping("login")
    public ResponseEntity<LoginBaseRespose<LoginResponse>> findLogin() {
        List<LoginT> login = loginService.findLogin();
        LoginBaseRespose<LoginResponse> loginBaseResponse = new LoginBaseRespose<>();
        loginBaseResponse.setCode(1);
        loginBaseResponse.setMessage("success");
        loginBaseResponse.setData(LoginResponse.builder()
                .login(login)
                .build());
        return ResponseEntity.ok(loginBaseResponse);
    }

}
