package com.example.trainningspring.btvn.model.entity;

public interface FullUserInfo {
    int getId();

    String getHoten();

    int getNamsinh();

    String getDiachi();

}
