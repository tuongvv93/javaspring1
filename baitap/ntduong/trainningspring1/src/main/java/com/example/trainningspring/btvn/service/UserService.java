package com.example.trainningspring.btvn.service;

import com.example.trainningspring.btvn.model.dto.user.UserCreateRequest;
import com.example.trainningspring.btvn.model.entity.User;
import com.example.trainningspring.btvn.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {
    @Autowired
    UserRepository userRepository;

    public List<User> findAllUser(){
        return userRepository.findAllUser();
    }
    public User findById(Integer id){
        return userRepository.findById(id);
    }

    public User findUser(String username, String password){
        return userRepository.findUser(username, password);
    }
    public User insertNewUser(String username, String password){
        return userRepository.insertNewUser(username,password);
    }
    public int createUser(UserCreateRequest userCreateRequest){
        return userRepository.saveUser(User.builder()
                .username(userCreateRequest.getUsername())
                .password(userCreateRequest.getPassword())
                .build());
    }
}
