package com.example.trainningspring.btvn.repository.database;

import com.example.trainningspring.btvn.model.entity.UserInfo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserInfoDB extends JpaRepository<UserInfo, Integer> {
}
