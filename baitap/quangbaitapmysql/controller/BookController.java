package com.example.demospring1.lern3.controller;

import com.example.demospring1.lern3.base.BaseResponse;
import com.example.demospring1.lern3.model.db.BookResponse;
import com.example.demospring1.lern3.model.db.Books;
import com.example.demospring1.lern3.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "mysql")
public class BookController {
    @Autowired
    BookService bookService;
    @RequestMapping("/books")
    public ResponseEntity<BaseResponse<BookResponse>> findAllBooks() {
        List<Books> books = bookService.findAllBooks();
        BaseResponse<BookResponse> baseResponse = new BaseResponse<>();
        baseResponse.setCode(1);
        baseResponse.setMessage("success");
        baseResponse.setData(BookResponse.builder()
                .books(books)
                .build());
        return ResponseEntity.ok(baseResponse);
    }
}
