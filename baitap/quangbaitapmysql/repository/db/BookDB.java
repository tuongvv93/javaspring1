package com.example.demospring1.lern3.repository.db;

import com.example.demospring1.lern3.model.db.Books;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookDB extends JpaRepository<Books, Integer> {

}