package com.example.demospring1.lern3.repository;


import com.example.demospring1.lern3.model.db.Books;
import com.example.demospring1.lern3.repository.db.BookDB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class BookRepository {
     @Autowired
    BookDB bookDB;
     public List<Books> findAllBooks() {
            return bookDB.findAll();
     }
}
