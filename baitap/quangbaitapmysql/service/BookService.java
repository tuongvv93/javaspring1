package com.example.demospring1.lern3.service;

import com.example.demospring1.lern3.model.db.Books;
import com.example.demospring1.lern3.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class BookService {
    @Autowired
    BookRepository bookRepository;
    public List<Books> findAllBooks() {
        return bookRepository.findAllBooks();
    }
}
