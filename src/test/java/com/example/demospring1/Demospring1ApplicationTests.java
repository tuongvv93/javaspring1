package com.example.demospring1;

import com.example.demospring1.learnjpa.repository.BookRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class Demospring1ApplicationTests {

	@Autowired
	BookRepository bookRepository;

	@Test
	void contextLoads() {
		int id = bookRepository.saveBook(com.example.demospring1.learnjpa.model.db.Books.builder()
				.name("sach so 2")
				.price(1000f)
				.categoryId(1)
				.build());

		System.out.println(" ---------> " + id);

	}

	@Test
	void testInsertBookRepository() {
		int id = bookRepository.saveBook(com.example.demospring1.learnjpa.model.db.Books.builder()
				.name("sach so 2")
				.price(1000f)
				.categoryId(1)
				.build());
		com.example.demospring1.learnjpa.model.db.Books books = bookRepository.findBookWithId(id);

		Assertions.assertEquals(1, books.getCategoryId());
		Assertions.assertEquals("sach so 2", books.getName());
		Assertions.assertEquals(1000f, books.getPrice());
	}

}
