package com.example.demospring1;

import com.example.demospring1.learnjpamongo.model.enity.BookMongo;
import com.example.demospring1.learnjpamongo.model.enity.CategoriesMongo;
import com.example.demospring1.learnjpamongo.repository.BookMongoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class TestMongo {

    @Autowired
    BookMongoRepository bookMongoRepository;

    @Test
    void contextLoads() {
    }

    @Test
    void testInsertBookRepository() {
        BookMongo bookMongo = bookMongoRepository.insert(BookMongo.builder()
                .name("sach so 2")
                .price(1000f)
                .category(CategoriesMongo.builder()
                        .id("xxxxxxx")
                        .name("Trinh thám")
                        .nameSearch("trinh tham")
                        .description("mo ta .....")
                        .build())
                .build());

        List<BookMongo> bookMongoList = bookMongoRepository.findAllBooks();
        System.out.println(bookMongoList.size());
//        Assertions.assertEquals(1, books.getCategoryId());
//        Assertions.assertEquals("sach so 2", books.getName());
//        Assertions.assertEquals(1000f, books.getPrice());
    }

}
