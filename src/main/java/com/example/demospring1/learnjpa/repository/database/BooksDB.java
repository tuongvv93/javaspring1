package com.example.demospring1.learnjpa.repository.database;

import com.example.demospring1.learnjpa.model.enity.BooksCategories;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface BooksDB extends JpaRepository<com.example.demospring1.learnjpa.model.db.Books, Integer> {

    // native query
    @Query(value = "select * from books c order by ?3 ?4 limit ?1 offset ?2", nativeQuery = true)
    List<com.example.demospring1.learnjpa.model.db.Books> findWithPage(int limit, int offset, String fieldSort, String sortDirection);

    @Query(value = "select count(*) from books", nativeQuery = true)
    Long countBook();

    //JPA
    @Query(value = "select c from Books c")
    Page<com.example.demospring1.learnjpa.model.db.Books> findWithPageJPA(Pageable pageable);

    @Query(value = "select books.id, books.name as bookName, books.price, categories.name as categoryName from books join categories where books.category_id = categories.id and books.id = ?1", nativeQuery = true)
    List<BooksCategories> findBookCategories(int id);

    @Query(value = "insert into books (price, name, category_id) values (?1, ?2, ?3); select LAST_INSERT_ID()", nativeQuery = true)
    void insertData(float price, String name, int categoryId);

    @Query(value = "select LAST_INSERT_ID() from books", nativeQuery = true)
    int selectLastId();
}