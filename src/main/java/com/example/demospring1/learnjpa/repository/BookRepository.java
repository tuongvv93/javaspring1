package com.example.demospring1.learnjpa.repository;

import com.example.demospring1.learnjpa.model.dto.book.BooksResponse;
import com.example.demospring1.learnjpa.model.enity.BooksCategories;
import com.example.demospring1.learnjpa.repository.database.BooksDB;
import com.example.demospring1.learnjpa.repository.database.CategoriesDB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;
import java.util.Optional;

@Repository
public class BookRepository {
    @Autowired
    BooksDB booksDB;
    @Autowired
    CategoriesDB categoriesDB;
    @PersistenceContext
    private EntityManager entityManager;

    public List<com.example.demospring1.learnjpa.model.db.Books> findAllBooks() {
        return booksDB.findAll();
    }

    public BooksResponse findWithPage(
            int pageSize, int pageIndex, String fieldSort, String sortDirection) {
        List<com.example.demospring1.learnjpa.model.db.Books> list = booksDB.findWithPage(pageSize, (pageIndex - 1) * pageSize, fieldSort, sortDirection);
        long totalRecord = booksDB.countBook();
        int totalPage = (int) (totalRecord / pageSize);
        if (totalRecord % pageSize > 0) {
            totalPage++;
        }
        return BooksResponse.builder()
                .total(totalPage)
                .pageIndex(pageIndex)
                .pageSize(pageSize)
                .books(list)
                .build();
    }

    public BooksResponse findWithPageJPA(
            int pageSize, int pageIndex, String fieldSort, String sortDirection) {
        Sort.Direction direction = Sort.Direction.DESC;
        if ("asc".equals(sortDirection)) {
            direction = Sort.Direction.ASC;
        }
        Pageable pageable = PageRequest.of(pageIndex, pageSize).withSort(Sort.by(direction, fieldSort));
        Page<com.example.demospring1.learnjpa.model.db.Books> page = booksDB.findWithPageJPA(pageable);
        return BooksResponse.builder()
                .total(page.getTotalPages())
                .pageIndex(pageIndex + 1)
                .pageSize(pageSize)
                .books(page.getContent())
                .build();
    }

    public List<BooksCategories> findBookCategories(int id) {
        return booksDB.findBookCategories(id);
    }

    public int saveBook(com.example.demospring1.learnjpa.model.db.Books book) {
        EntityManager entity = entityManager.getEntityManagerFactory().createEntityManager();
        try {
            entity.getTransaction().begin();
            // query 1
            Query query = entity.createNativeQuery("insert into books (price, name, category_id) values (:price, :name, :categoryId)");
            query.setParameter("categoryId", book.getCategoryId());
            query.setParameter("price", book.getPrice());
            query.setParameter("name", book.getName());
            query.executeUpdate();

            // query 2
            Query query2 = entity.createNativeQuery("select LAST_INSERT_ID()");
            int id = Integer.parseInt(query2.getSingleResult().toString());

            entity.getTransaction().commit();
            return id;
        } catch (Exception e) {
            entity.getTransaction().rollback();
        }
        return -1;
    }

    public int save(com.example.demospring1.learnjpa.model.db.Books book) {
        com.example.demospring1.learnjpa.model.db.Books bookRep = booksDB.save(book);
        if (bookRep == null) {
            return -1;
        }
        return bookRep.getId();
    }

    public com.example.demospring1.learnjpa.model.db.Books findBookWithId(int id) {
        Optional<com.example.demospring1.learnjpa.model.db.Books> optionalBooks = booksDB.findById(id);
        return optionalBooks.orElse(null);
    }
}
