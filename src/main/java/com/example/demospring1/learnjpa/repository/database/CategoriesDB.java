package com.example.demospring1.learnjpa.repository.database;

import com.example.demospring1.learnjpa.model.enity.Categories;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoriesDB extends JpaRepository<Categories, Integer> {

}
