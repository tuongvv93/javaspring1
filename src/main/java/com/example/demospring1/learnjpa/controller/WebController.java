package com.example.demospring1.learnjpa.controller;

import com.example.demospring1.learnjpa.controller.base.BaseController;
import com.example.demospring1.learnjpa.model.base.BaseResponse;
import com.example.demospring1.learnjpa.model.dto.book.PersonForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@RestController
@RequestMapping("validate")
public class WebController extends BaseController {
    @Autowired
    TaskExecutor taskExecutor;

    @PostMapping("checkPersonInfo")
    public ResponseEntity<BaseResponse<String>> checkPersonInfo(@Valid @RequestBody PersonForm personForm, BindingResult bindingResult) {
        BaseResponse<String> baseResponse = new BaseResponse();
        if (bindingResult.hasErrors()) {
            baseResponse.setCode(-1);
            baseResponse.setMessage(bindingResult.getFieldError().getDefaultMessage());
            return ResponseEntity.ok(baseResponse);
        }
        baseResponse.setCode(1);
        baseResponse.setMessage("Success");
        return ResponseEntity.ok(baseResponse);
    }

    @GetMapping("exception")
    public ResponseEntity exceptionTest() throws Exception {
        if (1 == 1) {
           throw new Exception("Lỗi xyz");
        }
        return ResponseEntity.internalServerError().body("err");
    }

    @GetMapping("taskExcutor")
    public ResponseEntity taskExcutor() {
        // api này chậm
        // code tương tác với database này có thể chạy ngầm được
        // chạy thread
        // những tác vụ k cần thiết trả về cho người dùng thì mình để trong thread

        taskExecutor.execute(new Runnable() {
            @Override
            public void run() {
                // không cần thiết trả về client
                slowFunct();
            }
        });
        return ResponseEntity.ok("ok");
    }

    private void slowFunct() {
        for (int i = 0; i < 1000000; i ++) {
            System.out.println("xxxxx");
        }
    }
}