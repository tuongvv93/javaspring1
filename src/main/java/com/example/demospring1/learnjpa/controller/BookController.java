package com.example.demospring1.learnjpa.controller;

import com.example.demospring1.learnjpa.controller.base.BaseController;
import com.example.demospring1.learnjpa.model.base.BaseResponse;
import com.example.demospring1.learnjpa.model.dto.book.BookCreateRequest;
import com.example.demospring1.learnjpa.model.dto.book.BookCreateResponse;
import com.example.demospring1.learnjpa.model.dto.book.BooksResponse;
import com.example.demospring1.learnjpa.model.enity.BooksCategories;
import com.example.demospring1.learnjpa.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
// cho phep web code bang nodejs call len api cua minh, va minh can chan theo domain
// chi ng dung tu website local vao dc.
@CrossOrigin(origins = "http://localhost:8081")
@RequestMapping(path = "mysql")
public class BookController extends BaseController {
    @Autowired
    BookService bookService;

    @RequestMapping("/books")
    public ResponseEntity<BaseResponse<BooksResponse>> findAllBooks() {
        BaseResponse<BooksResponse> baseResponse = new BaseResponse<>();
        List<com.example.demospring1.learnjpa.model.db.Books> books = bookService.findAllBooks();

        BooksResponse booksResponse = BooksResponse.builder().books(books).build();

        baseResponse.setCode(1);
        baseResponse.setMessage("success");
        baseResponse.setData(booksResponse);

        return ResponseEntity.ok(baseResponse);
    }

    @RequestMapping("/books-with-page")
    public ResponseEntity<BaseResponse<BooksResponse>> findWithPage(
            @RequestParam("pageIndex") int pageIndex,
            @RequestParam("pageSize") int pageSize,
            @RequestParam(value = "fieldSort") String fieldSort,
            @RequestParam(value = "sortDirection") String sortDirection
    ) {
        BaseResponse<BooksResponse> baseResponse = new BaseResponse<>();
        BooksResponse booksResponse = bookService.findWithPage(pageIndex, pageSize, fieldSort, sortDirection);
        baseResponse.setCode(1);
        baseResponse.setMessage("success");
        baseResponse.setData(booksResponse);

        return ResponseEntity.ok(baseResponse);
    }

    @RequestMapping(value = "/books-with-page-jpa")
    public ResponseEntity<BaseResponse<BooksResponse>> findWithPageJPA(
            @RequestParam("pageIndex") int pageIndex,
            @RequestParam("pageSize") int pageSize,
            @RequestParam(value = "fieldSort") String fieldSort,
            @RequestParam(value = "sortDirection") String sortDirection
    ) {
        BaseResponse<BooksResponse> baseResponse = new BaseResponse<>();
        BooksResponse booksResponse = bookService.findWithPageJPA(pageIndex, pageSize, fieldSort, sortDirection);
        baseResponse.setCode(1);
        baseResponse.setMessage("success");
        baseResponse.setData(booksResponse);

        return ResponseEntity.ok(baseResponse);
    }

    @GetMapping("/{id}/books-categories/")
    public ResponseEntity<BaseResponse<List<BooksCategories>>> findBookCategories(
            @PathVariable("id") int id) {
        BaseResponse<List<BooksCategories>> baseResponse = new BaseResponse<>();
        List<BooksCategories> list = bookService.findBookCategories(id);
        baseResponse.setCode(1);
        baseResponse.setMessage("success");
        baseResponse.setData(list);

        return ResponseEntity.ok(baseResponse);
    }

    // {
    //     "name": "sach giao khoa 1",
    //     "price: 10000,
    //     "categoryId": 1
    // }
    @PostMapping("book")
    public ResponseEntity<BaseResponse<BookCreateResponse>> createBook(@RequestBody BookCreateRequest bookCreateRequest) {
        BaseResponse<BookCreateResponse> baseResponse = new BaseResponse();
        int id = bookService.createBook(bookCreateRequest);
        if (id > 0) {
            baseResponse.setCode(1);
            baseResponse.setMessage("success");
        } else {
            baseResponse.setCode(0);
            baseResponse.setMessage("can not create book");
        }
        baseResponse.setData(BookCreateResponse.builder()
                .id(id)
                .build());
        return ResponseEntity.ok(baseResponse);
    }

}
