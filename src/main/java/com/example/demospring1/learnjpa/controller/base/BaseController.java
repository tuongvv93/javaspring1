package com.example.demospring1.learnjpa.controller.base;

import com.example.demospring1.learnjpa.model.base.BaseResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;

public class BaseController {

    @ExceptionHandler({ NumberFormatException.class })
    public void handleException() {
        System.out.println(" ----------  exception in here ");
    }

    @ExceptionHandler(Exception.class)
    protected ResponseEntity<BaseResponse<String>> handleEntityNotFound(
            Exception ex) {
        BaseResponse<String> baseResponse = new BaseResponse<>();
        baseResponse.setCode(-1);
        baseResponse.setMessage(ex.getMessage());
        return ResponseEntity.ok(baseResponse);
    }

}
