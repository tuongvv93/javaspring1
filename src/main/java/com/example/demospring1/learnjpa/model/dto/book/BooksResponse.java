package com.example.demospring1.learnjpa.model.dto.book;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BooksResponse {
    int total;
    int pageSize;
    int pageIndex;
    private List<com.example.demospring1.learnjpa.model.db.Books> books;
}
