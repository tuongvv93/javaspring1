package com.example.demospring1.learnjpa.model.enity;

public interface BooksCategories {
    int getId();

    String getBookName();

    String getCategoryName();

    Float getPrice();
}
