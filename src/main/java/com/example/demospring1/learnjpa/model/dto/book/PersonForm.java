package com.example.demospring1.learnjpa.model.dto.book;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PersonForm {

	@NotNull(message = "name không được để trống")
	@Size(min=2, max=30, message = "Name phải từ 2 đến 30 kí tự")
	private String name;

	@NotNull
	@Min(value = 18, message = "tuổi phải lớn hơn 18")
	private Integer age;
}