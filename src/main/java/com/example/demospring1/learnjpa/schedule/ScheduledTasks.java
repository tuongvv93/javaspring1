package com.example.demospring1.learnjpa.schedule;

import com.example.demospring1.learnjpamongo.model.enity.BookMongo;
import com.example.demospring1.learnjpamongo.repository.BookMongoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ScheduledTasks {
	@Autowired
	BookMongoRepository bookMongoRepository;

	@Scheduled(cron = "0 0 23 * *")
	public void reportCurrentTime() {
		// 11h đêm hàng ngày chạy code này
		//bookMongoRepository.findAllBooks();
	}
}