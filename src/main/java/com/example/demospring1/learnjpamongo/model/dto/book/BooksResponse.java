package com.example.demospring1.learnjpamongo.model.dto.book;

import com.example.demospring1.learnjpamongo.model.enity.BookMongo;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BooksResponse {
    int total;
    int pageSize;
    int pageIndex;
    private List<BookMongo> books;
}
