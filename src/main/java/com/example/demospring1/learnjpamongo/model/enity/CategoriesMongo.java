package com.example.demospring1.learnjpamongo.model.enity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "categories")
@CompoundIndexes({
        @CompoundIndex(name = "idx1", def = "{'nameSearch'}"),
})
public class CategoriesMongo {
    @Id
    private String id;
    private String name;  // Trinh thám
    private String nameSearch;  // trinh tham
    private String description;
}
