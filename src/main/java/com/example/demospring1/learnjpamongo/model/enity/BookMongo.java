package com.example.demospring1.learnjpamongo.model.enity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.*;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "books")
@CompoundIndexes({
        @CompoundIndex(name = "idx1", def = "{'name', 'category.id'}"),
        @CompoundIndex(name = "idx2", def = "{'name'}"),
        @CompoundIndex(name = "idx3", def = "{'price'}")
})
public class BookMongo {
    @Id
    private String id;
    private String name;
    private Float price;
    private CategoriesMongo category;
}
