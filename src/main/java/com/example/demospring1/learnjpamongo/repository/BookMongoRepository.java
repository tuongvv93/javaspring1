package com.example.demospring1.learnjpamongo.repository;

import com.example.demospring1.learnjpamongo.model.dto.book.BooksResponse;
import com.example.demospring1.learnjpamongo.model.enity.BookMongo;
import com.example.demospring1.learnjpamongo.repository.database.BooksMongoDB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.*;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class BookMongoRepository {
    @Autowired
    BooksMongoDB booksMongoDB;
    // disable  https://www.baeldung.com/spring-data-mongodb-tutorial
    @Autowired
    MongoTemplate mongoTemplate;

    public List<BookMongo> findAllBooks() {
        return booksMongoDB.findAll();
    }

    public BooksResponse findWithPageJPA(
            int pageSize, int pageIndex, String fieldSort, String sortDirection) {
        Sort.Direction direction = Sort.Direction.DESC;
        if ("asc".equals(sortDirection)) {
            direction = Sort.Direction.ASC;
        }
        Pageable pageable = PageRequest.of(pageIndex, pageSize).withSort(Sort.by(direction, fieldSort));
        Page<BookMongo> page = booksMongoDB.findWithPage(pageable);
        return BooksResponse.builder()
                .total(page.getTotalPages())
                .pageIndex(pageIndex + 1)
                .pageSize(pageSize)
                .books(page.getContent())
                .build();
    }

    public List<BookMongo> findAllBooksById(String id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        // { "id": "id"}
        return mongoTemplate.find(query, BookMongo.class);
    }

    public List<BookMongo> findAllBooksByIdPage(String id, int pageIndex, int pageSize) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        // { "id": "id"}
        query.limit(pageSize);
        query.skip((pageIndex - 1) * pageSize);
        long count = mongoTemplate.count(query, BookMongo.class);
        // phan trang tay o day
        return mongoTemplate.find(query, BookMongo.class);
    }

    // cach 1
    public BookMongo insert(BookMongo bookMongo) {
        return mongoTemplate.insert(bookMongo);
//        return booksMongoDB.save(bookMongo);
    }

    // cach 2
    public BookMongo insert2(BookMongo bookMongo) {
        return booksMongoDB.save(bookMongo);
    }

    // aggreate   -> using mongoTemplate
    public List<BookMongo> aggregateDemo(BookMongo bookMongo) {
        MatchOperation matchStage = Aggregation.match(new Criteria("category.id").is("sach giao khoa"));
        ProjectionOperation projectStage = Aggregation.project("catetegory.name", "name", "price", "id");

        Aggregation aggregation
                = Aggregation.newAggregation(matchStage, projectStage);

        AggregationResults<BookMongo> output
                = mongoTemplate.aggregate(aggregation, "books", BookMongo.class);
        return output.getMappedResults();
    }
}
