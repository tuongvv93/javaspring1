package com.example.demospring1.learnjpamongo.repository.database;

import com.example.demospring1.learnjpa.model.enity.Categories;
import com.example.demospring1.learnjpamongo.model.enity.CategoriesMongo;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CategoriesMongoDB extends MongoRepository<CategoriesMongo, String> {

}
