package com.example.demospring1.learnjpamongo.repository.database;

import com.example.demospring1.learnjpamongo.model.enity.BookMongo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BooksMongoDB extends MongoRepository<BookMongo, String> {
    @Query("{ name: {$eq: ?1}}")
    List<BookMongo> findByName(String name);

    @Query("{}")
    Page<BookMongo> findWithPage(Pageable pageable);
}