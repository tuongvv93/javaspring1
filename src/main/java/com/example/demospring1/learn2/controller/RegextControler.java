package com.example.demospring1.learn2.controller;

import com.example.demospring1.learn2.service.RegexService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping({"/regex"})
public class RegextControler {
    @Autowired
    RegexService regexService;

    @GetMapping("/index")
    public String Regex(
            @RequestParam(value = "testData", required = false) String testData,
            @RequestParam(value = "regexValue", required = false) String regexValue,
            Model model) {

        if (regexValue == null || testData == null) {
            model.addAttribute("list", new ArrayList<>());
            return "regex";
        }
        List<String> list = regexService.findRegex(regexValue, testData);

        model.addAttribute("list", list);
        model.addAttribute("existMatch", list.size() > 0 ? "Exist" : "Not exist");
        return "regex";
    }
}
