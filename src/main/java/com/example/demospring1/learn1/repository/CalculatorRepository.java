package com.example.demospring1.learn1.repository;

import org.springframework.stereotype.Repository;

@Repository
public class CalculatorRepository {
    public int add(int a, int b) {
        return a + b;
    }
}
