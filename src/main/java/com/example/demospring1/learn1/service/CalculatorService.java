package com.example.demospring1.learn1.service;

import com.example.demospring1.learn1.repository.CalculatorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CalculatorService {
    @Autowired
    CalculatorRepository calculatorRepository;

    public Integer add(Integer a, Integer b) {
        if (a == null || b == null) {
            return null;
        }
        return calculatorRepository.add(a, b);
    }
}
