package com.example.demospring1.learn1.controller;

import com.example.demospring1.learn1.model.Student;
import com.example.demospring1.learn1.service.CalculatorService;
import com.example.demospring1.learn1.utils.LanguageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

@org.springframework.stereotype.Controller
public class CalculatorController {

    @Autowired
    CalculatorService calculatorService;

    @GetMapping("calculator")
    public String calculator(
            @RequestParam(value = "a", required = false) Integer a,
            @RequestParam(value = "b", required = false) Integer b,
            @RequestHeader(value = "Authorization", required = false) String authorization,
            Model model) {

        String language = "";
        String lang = LanguageUtils.map.get("vi");
        if (LanguageUtils.map.containsKey(language)) {
            lang = LanguageUtils.map.get(language);
        }
        var locale = Locale.forLanguageTag(lang);
        var messages = ResourceBundle.getBundle("message", locale);
        System.out.print(messages.getString("hello") + " ");

        if (a == null || b == null) {
            return "calculator";
        }

        System.out.println("a : " + a + " b: " + b);
        model.addAttribute("sum", calculatorService.add(a, b));

        List<Student> students = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            students.add(new Student("Student " + i, i * 10 + 5));
        }
        model.addAttribute("students", students);
        return "calculator";
    }

}
