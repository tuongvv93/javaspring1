package com.example.demospring1.learn1.controller;

import com.example.demospring1.learn1.model.Student;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@org.springframework.stereotype.Controller
public class Controller {

    @GetMapping("greeting")
    public String greeting(Model model) {
        model.addAttribute("name", "http://google.com");
        return "index";
    }

    @GetMapping("jsonxxxxx")
    public ResponseEntity<Student> demoJson() {

        return ResponseEntity.ok(new Student("Name", 18));
    }

}
