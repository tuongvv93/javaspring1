package com.example.demospring1.learn1.utils;

import java.util.HashMap;
import java.util.Map;

public class LanguageUtils {
    public static Map<String, String> map = new HashMap<>();

    static {
        map.put("vi", "vi-VN");
        map.put("en", "en-US");
    }

}
